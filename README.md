### DDNS_UPDATER

I created this simple tool after I changed the machine I used for my homelab and missed migrating the crontab for the ddns service I use, I decided to move it to simple container instead of running curl in a cron job.

if you have a minikube/k3s or any other k8s, you can deploy it directly to the cluster:

```
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ddns-updater
  labels: 
    app: ddns-updater
spec:
  replicas: 1
  selector:
    matchLabels: 
      k8s-app: ddns-updater
  template:
    metadata:
      labels: 
        k8s-app: ddns-updater
    spec:
      containers:
        - name: jddns-updater
          image: registry.gitlab.com/sabd/ddns_updater:latest
          env:
          - name: URL
            value: "http://sync.afraid.org/u/$TOKEN/"
          - name: FREQ
            value: "300"          
          ports:
          - containerPort: 9100
            name: metrics
```