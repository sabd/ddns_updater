package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	"github.com/jessevdk/go-flags"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var opt struct {
	URL         string `short:"u" long:"url" env:"URL" description:"full url including token of dynamic dns service"`
	ReqFreq     int    `short:"f" long:"freq" env:"FREQ" default:"300" description:"Probe frequency in seconds"`
	MetricsPort int    `short:"m" long:"metrics_port" default:"9100" description:"Prometheus Metrics port"`
}

var RespStatus = prometheus.NewCounterVec(prometheus.CounterOpts{
	Name: "response_code",
	Help: "Count of different response codes responses",
}, []string{"response_code"})

func main() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	prometheus.MustRegister(RespStatus)

	parseFlags()

	http.Handle("/metrics", promhttp.Handler())
	go http.ListenAndServe(fmt.Sprintf(":%d", opt.MetricsPort), nil)

	ticker := time.NewTicker(time.Duration(opt.ReqFreq) * time.Second)

	quit := make(chan struct{})

	for {
		select {
		case <-ticker.C:
			go sendReq(opt.URL)
		case <-quit:
			ticker.Stop()
			return
		}
	}
}

func sendReq(url string) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Error().Err(err).Msg("Failed parsing url")
		return
	}
	req.Header.Add("Accept", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("Error sending url")
		return
	}
	RespStatus.With(prometheus.Labels{"response_code": fmt.Sprintf("%v", resp.StatusCode)}).Inc()

	defer resp.Body.Close()

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error().Err(err).Msg("Error parsing response body")
	} else {
		log.Info().Msg(string(b))
	}
}

func parseFlags() {
	_, e := flags.Parse(&opt)
	if e != nil {
		log.Fatal().Err(e).Msg("Error parsing arguments")
	}
	log.Info().Msgf("received params,url:%s,frequency:%d", opt.URL, opt.ReqFreq)
}
